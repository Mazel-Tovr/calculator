package com.epam.calculaor;

public interface Calculable
{
    double toCalculate(String string);

}
