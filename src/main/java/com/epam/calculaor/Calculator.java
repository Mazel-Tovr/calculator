package com.epam.calculaor;

public class Calculator implements Calculable {

    @Override
    public double toCalculate(String string) {
        string = string.replaceAll(" ", "");
        if (string.contains("sqrt")) {
            string = getRidOfSqrt(string);
        }
        StringBuilder tempString = new StringBuilder(string);
        //Костыль ☺
        while (!hasResult(tempString.toString())) {
            int operatorIndex = getIndexOfPriorityOperator(tempString.toString()) > 0 ? getIndexOfPriorityOperator(tempString.toString()) : getRightIndexOfAnyOperator(0, tempString.toString());
            int indexOfFirstNumber = getLeftIndexOfAnyOperator(operatorIndex, tempString.toString());
            int indexOfSecondNumber = getRightIndexOfAnyOperator(operatorIndex, tempString.toString());

            indexOfFirstNumber = indexOfFirstNumber == 0 ? 0 : indexOfFirstNumber + 1;

            double firstNumber = Double.parseDouble(tempString.substring(indexOfFirstNumber, operatorIndex));
            double secondNumber = Double.parseDouble(tempString.substring(operatorIndex + 1, indexOfSecondNumber));

            double result = operate(firstNumber, secondNumber, tempString.charAt(operatorIndex));

            tempString.replace(indexOfFirstNumber, indexOfSecondNumber, String.valueOf(result));

        }
        return Double.parseDouble(tempString.toString());
    }

    private double operate(double firstNumber, double secondNumber, char operator) {
        switch (operator) {
            case '+':
                return firstNumber + secondNumber;
            case '-':
                return firstNumber - secondNumber;
            case '/':
                return firstNumber / secondNumber;
            case '*':
                return firstNumber * secondNumber;
            default:
                return 0;
        }
    }

    private double operate(double sqrtNumber) {
        return Math.sqrt(sqrtNumber);
    }


    private boolean hasResult(String s) {
        try {
            Double.parseDouble(s);
            return true;
        } catch (NumberFormatException e) {
            return false;
        }
    }


    private String getRidOfSqrt(String string) {
        StringBuilder tempString = new StringBuilder(string);
        while (tempString.indexOf("sqrt") >= 0) {
            int start = tempString.indexOf("sqrt");
            int endIndex = getRightIndexOfAnyOperator(start, tempString.toString());

            String subString = tempString.substring(start, endIndex);

            double result = operate(Double.parseDouble(subString.replace("sqrt", "")));

            tempString.replace(start, endIndex, String.valueOf(result));

        }
        return tempString.toString();
    }

    private int getLeftIndexOfAnyOperator(int startIndex, String s) {
        for (int i = startIndex - 1; i > 0; i--) {
            char currentChar = s.charAt(i);
            if (currentChar == '-' || currentChar == '+' || currentChar == '*' || currentChar == '/') {
                return i;
            }
        }
        return 0;
    }

    private int getRightIndexOfAnyOperator(int startIndex, String s) {
        for (int i = startIndex + 1; i < s.length(); i++) {
            char currentChar = s.charAt(i);
            if (currentChar == '-' || currentChar == '+' || currentChar == '*' || currentChar == '/') {
                return i;
            }
        }
        return s.length();
    }

    private int getIndexOfPriorityOperator(String s) {
        for (int i = 0; i < s.length(); i++) {
            char currentChar = s.charAt(i);
            if (currentChar == '*' || currentChar == '/') {
                return i;
            }
        }
        return -1;
    }

}
