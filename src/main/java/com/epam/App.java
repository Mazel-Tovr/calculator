package com.epam;

import com.epam.calculaor.Calculable;
import com.epam.calculaor.Calculator;
import com.epam.validation.Validatable;
import com.epam.validation.Validation;

import java.util.Scanner;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) {


        Calculable calculable = new Calculator();
        Validatable validatable = new Validation();
        System.out.println("Калькулятор");
        System.out.println("Допустимые операторы +,-,*,/\nТак же допустимо использование sqrtЧисло");
        Scanner scanner = new Scanner(System.in);
        while (true) {
            try {
                String expression;
                do {
                    System.out.println("Введите выражение  ");
                    expression = scanner.nextLine();
                    if (validatable.inExpressionValid(expression)) {
                        break;
                    } else {
                        System.out.println("Ошибка, введите выражение повторно");
                    }
                } while (true);

                double result = calculable.toCalculate(expression);
                System.out.println(result == Double.POSITIVE_INFINITY ? "Ошибка, деление на 0" : result);


            } catch (Exception e) {
                System.out.println(e.getMessage());
            }

        }
    }
}
