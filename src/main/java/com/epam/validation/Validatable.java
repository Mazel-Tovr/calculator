package com.epam.validation;

public interface Validatable
{
    boolean inExpressionValid(String string);
}
