package com.epam.validation;

public class Validation implements Validatable
{

    @Override
    public boolean inExpressionValid(String string)
    {
        string = string.replaceAll(" ","");
        return (!isContainsUnacceptableCharacters(string)) && (operatorCountCheck(string));
    }

    private boolean isContainsUnacceptableCharacters(String string) {
        String[] tempo = string.split("(\\+)|(-)|(\\*)|(/)");
        try
        {
            for (String number:tempo)
            {
              if(!number.matches("sqrt\\d*\\.?\\d+"))
                Double.parseDouble(number);
            }
        }
        catch (NumberFormatException e)
        {
            return true;
        }
            return false;
    }

    private boolean operatorCountCheck(String string)
    {
        int operatorsCount = 0;
        for (char symbol: string.toCharArray())
        {
                if(symbol == '-'||symbol == '+'||symbol == '/'||symbol == '*')
                    operatorsCount++;
        }
        return string.split("(\\+)|(-)|(\\*)|(/)").length == operatorsCount + 1;
    }


}
