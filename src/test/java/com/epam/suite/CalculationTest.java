package com.epam.suite;

import com.epam.calculaor.Calculable;
import com.epam.calculaor.Calculator;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

public class CalculationTest {
    Calculable calculable = new Calculator();

    @Test
    public void calculationTestSumСommutativity() {
        double EXPECTED_SUMMARISED  = 9.0;
        assertEquals(EXPECTED_SUMMARISED ,calculable.toCalculate("4+5"), 0.0);
        assertEquals(EXPECTED_SUMMARISED, calculable.toCalculate("5+4"), 0.0);
    }
    @Test
    public void calculationZeroElementAddition()
    {
        double EXPECTED  = 54.0;
        assertEquals(EXPECTED ,calculable.toCalculate("54+0"), 0.0);
        assertEquals(EXPECTED, calculable.toCalculate("0+54"), 0.0);
    }

    @Test
    public void calculationMinusAnticommutativity() {
        double EXPECTED = -10.0;
        assertEquals(EXPECTED, calculable.toCalculate("10-20"), 0.0);
        assertEquals(EXPECTED, calculable.toCalculate("20-10")*(-1), 0.0);//калькулятор не предусматривает наличие скобок
    }

    @Test
    public void calculationSubtractZero()
    {
        double EXPECTED = 21;
        assertEquals(EXPECTED,calculable.toCalculate("21-0"),0.0);
        assertEquals(EXPECTED*(-1),calculable.toCalculate("0-21"),0.0);
    }

    @Test
    public void calculationMultiplicationСommutativity() {

        double EXPECTED = 135;
        assertEquals(EXPECTED, calculable.toCalculate("15*9"), 0.0);
        assertEquals(EXPECTED, calculable.toCalculate("9*15"), 0.0);
    }

    @Test
    public void calculationDivisionAnticommutativity() {

        assertNotEquals(calculable.toCalculate("31/351"), calculable.toCalculate("351/31"), 1e-9);
    }
    @Test
    public void negativeTestDivideByZero() {
        assertEquals(Double.POSITIVE_INFINITY, calculable.toCalculate("1 / 0"), 0.0);
    }
    @Test
    public void calculationSqrt() {
        assertEquals(Math.sqrt(64), calculable.toCalculate("sqrt64"), 0.0);
    }

    @Test
    public void calculationRootOfFractalNumber() {
        assertEquals(Math.sqrt(64.4), calculable.toCalculate("sqrt64.4"), 0.0);
    }

    @Test
    public void calculationDouble() {
        assertEquals(54.31 + 21.51 - 31.13, calculable.toCalculate("54.31 + 21.51 - 31.13"), 1e-9);
    }

    @Test
    public void calculationMixed() {
        assertEquals(8 * 54.3 + 31.0 / 51.0 + Math.sqrt(4) * 54.0, calculable.toCalculate("8 * 54.3 + 31 / 51 + sqrt4 * 54"), 1e-9);
    }

    @Test
    public void calculationTrimInDependency() {
        assertEquals(873 + 31 - 63.0 / 31.0 * 313, calculable.toCalculate("  873     +   31   -  63.0/31.0*    313   "), 1e-9);
    }


}
