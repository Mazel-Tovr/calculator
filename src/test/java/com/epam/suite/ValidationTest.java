package com.epam.suite;

import com.epam.validation.Validatable;
import com.epam.validation.Validation;
import org.junit.Test;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class ValidationTest
{

    Validatable validatable = new Validation();


    @Test
    public void shouldAnswerTrueTwoNumbers()
    {
        assertTrue(validatable.inExpressionValid("441 + 35") );
    }

    @Test
    public void shouldAnswerSqrt()
    {
        assertTrue( validatable.inExpressionValid("sqrt4") );
    }

    @Test
    public void shouldAnswerTrueNumberAndSqrt()
    {
        assertTrue( validatable.inExpressionValid("10 + sqrt4") );
    }
    @Test
    public void shouldAnswerTrueSqrtAndNumber()
    {
        assertTrue( validatable.inExpressionValid("sqrt4 + 2") );
    }

    @Test
    public void shouldAnswerTrueWithTwoAndDouble()
    {
        assertTrue(validatable.inExpressionValid("75.41 + 31.31"));
    }


    @Test
    public void shouldAnswerTrueSqrtAndNumberAndDouble()
    {
        assertTrue( validatable.inExpressionValid("sqrt4 + 2 + 123.31") );
    }

    @Test
    public void shouldAnswerTrueMultiplication()
    {
        assertTrue(validatable.inExpressionValid("31 * 64"));
    }

    @Test
    public void shouldAnswerTrueDivision()
    {
        assertTrue(validatable.inExpressionValid("31 / 64"));
    }


    @Test
    public void shouldAnswerTrueWithMultiplicationAndDivision()
    {
        assertTrue(validatable.inExpressionValid("862 * 3123 / 622"));
    }

    @Test
    public void shouldAnswerTrueMixed()
    {
        assertTrue( validatable.inExpressionValid("31 + 5 - 41.3 * 51 / 62") );
    }

    @Test
    public void shouldAnswerTrueTrimInDependency()
    {
        assertTrue(validatable.inExpressionValid("31+3    -    3*3    /    sqrt16-sqrt31"));
    }

    @Test
    public void shouldAnswerFalseMissingNumber()
    {
        assertFalse( validatable.inExpressionValid("31 + - 41.3"));
    }

    @Test
    public void shouldAnswerFalseTwoDots()
    {
        assertFalse( validatable.inExpressionValid("31 +  41.3. + 3") );
    }

    @Test
    public void shouldAnswerFalseLetterInExpression()
    {
        assertFalse(validatable.inExpressionValid("315 + 86k + 42 * 41l"));
    }

    @Test
    public void shouldAnswerFalseExtraSymbols()
    {
        assertFalse(validatable.inExpressionValid("31 / 31 + 31 : 97 - 31 & 31"));
    }

    @Test
    public void shouldAnswerFalseUnfinishedSqrt()
    {
        assertFalse(validatable.inExpressionValid("5131 / 311 + sqr31 - 3"));
    }

}
