package com.epam;

import static org.junit.Assert.assertTrue;

import com.epam.suite.CalculationTest;
import com.epam.suite.ValidationTest;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.Suite;


@RunWith(Suite.class)
@Suite.SuiteClasses(value = {ValidationTest.class, CalculationTest.class})
public class AppTest 
{
    /**
     * Rigorous Test :-)
     */
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }
}
